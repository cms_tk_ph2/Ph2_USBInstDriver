#include "ArdNanoController.h"

using namespace Ph2_UsbInst;

ArdNanoController::ArdNanoController ()
{
    LOG (INFO) << "Opening connection to Arduino Nano." ;
    fHandler = new ArduinoSerial ("/dev/ArduinoNano", 9600);
}
bool ArdNanoController::CheckArduinoState()
{
    LOG (INFO) << "Checking connection to Arduino Nano." ;
    int cCounter = 0 ;
    std::regex e ("(ARDUINO NANO READY)(.*)");
    std::string cReadString = "";

    do
    {
        //if ( fAsync ) cReadString = fHandler->devread();
        //else cReadString = fHandler_Simple->readString();
        cReadString = fHandler->devread();

        if ( std::regex_match (cReadString, e) )
        {
            LOG (DEBUG) << BOLDGREEN << "Read : " << cReadString << RESET  ;
            LOG (INFO) << BOLDGREEN << "Successfully connected to the Arduino NANO board." << RESET;
            return true;
        }

        cCounter ++;
    }
    while ( cCounter < 100 );

    //LOG (DEBUG) << BOLDRED << "Read : " << cReadString << RESET ;
    LOG (INFO) << BOLDRED << "Could not connect to the Arduino NANO board." << RESET;
    return false;

}
ArdNanoController::~ArdNanoController()
{
    if (fHandler) delete fHandler;
}

uint8_t ArdNanoController::Write (std::string pMessage)
{
    return fHandler->devwrite (pMessage);
}

std::string ArdNanoController::Read()
{
    return fHandler->devread();
}

void ArdNanoController::ControlLED (uint8_t pLedState)
{
    Write ( "LED:SEL " + std::to_string (pLedState) );
}

void ArdNanoController::ControlRelay (uint8_t pRelayState)
{
    Write ("REL:SEL " + std::to_string (pRelayState) );
}
uint8_t ArdNanoController::GetRelayState ()
{
    Write ("REL?");
    return std::atoi ( fReply.c_str() );
}

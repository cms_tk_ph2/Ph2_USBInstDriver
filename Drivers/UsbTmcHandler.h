/*

    \file                          UsbTmcHandler.h
    \brief                         Wrapper class to communicate with USB instruments via usbtmc and SCPI
    \author                        Georg Auzinger
    \version                       1.0
    \date                          22/09/2016
    Support :                      mail to : georg.auzinger@SPAMNOT.cern.ch

 */

#ifndef _USBTMCHANDLER__
#define _USBTMCHANDLER__
#include <iostream>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>
#include <string>
#include <sys/fcntl.h>
#include <unistd.h>
#include <regex>
#include <cerrno>
#include "../usbtmc/usbtmc.h"
#include "../Utils/easylogging++.h"
#include "../Utils/ConsoleColor.h"


namespace Ph2_UsbInst {

    class UsbTmcHandler
    {
      public:
        UsbTmcHandler (std::string pDevName);
        // Destructor needs to close fFile!
        ~UsbTmcHandler();

        // generic write method that accepts SCPI command as string!
        std::string devwrite (const std::string pMsg);
        // generic read method that accepts SCPI command as string!
        std::string devread ();
        //normal fire&forget write for SCPI commands without reply
        void simplewrite (const std::string pMsg);

      private:
        void  openFile ( int pMinorId, std::string pDevName);


      private:
        int fFile;
        bool fFileOpen;
        const static int buffsize = 1000;
        //char* buffer;
        //int actual;
        //int retval;
        //extern int errno;
        //int errornum;
        struct usbtmc_instrument fInst;
        struct usbtmc_attribute fAttr;
        uint32_t fNInst;
        uint32_t fMinorId;
    };
}





#endif

/*

    \file                          HMP4040Client.h
    \brief                         Client class to communicate with HMP4040 server/controller via TCP sockets provided by the ZeroMQ library
    \author                        Georg Auzinger
    \version                       1.0
    \date                          06/10/2016
    Support :                      mail to : georg.auzinger@SPAMNOT.cern.ch
 */

#ifndef _HMP404CLIENT_H__
#define _HMP404CLIENT_H__

#ifdef __ZMQ__
//the client code only makes sense if ZMQ is available
#include <iostream>
#include "../Utils/Client.h"
#include "../Utils/Messages.h"


namespace Ph2_UsbInst {

    //class MeasurementValues;

    class HMP4040Client : public Client
    {
      public:
        // struct to hold the latest read measurement values
        MeasurementValues fValues;
        //C-tor and D-tor
        HMP4040Client (std::string pHostname, int pPort) : Client (pHostname, pPort, "HMP4040")
        {
            LOG (INFO) << GREEN <<  "Instantiated new HMP4040 client on host " << YELLOW <<  pHostname << GREEN <<  " port " << YELLOW << pPort << RESET;
        }
        ~HMP4040Client()
        {}

        //methods to control the instrument
        bool Reset();
        bool StartMonitoring();
        bool StopMonitoring();
        bool PauseMonitoring();
        bool ResumeMonitoring();
        bool SetLogFileName (std::string pLogFileName);
        bool SetConfigFileName (std::string pConfigFileName);
        bool Configure();
        bool ToggleOutput (bool pState);
        bool GetLatestReadValues();
        bool MeasureValues();
        bool Quit();
      private:
        void parseValueString (std::string& pValueString);
    };

} // namespace Ph2_UsbInst



#endif
#endif

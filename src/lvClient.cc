#include <iostream>
#include <unistd.h>
#include <limits.h>
#include <signal.h>
#include "../HMP4040/HMP4040Client.h"
#include "../Utils/easylogging++.h"
#include "../Utils/argvparser.h"
#include "../Utils/ConsoleColor.h"
#include "boost/tokenizer.hpp"
#include "../Utils/Messages.h"
#ifdef __ZMQ__
#include <zmq.hpp>
#include "../Utils/zmqutils.h"
#endif

using namespace Ph2_UsbInst;
using namespace CommandLineProcessing;

INITIALIZE_EASYLOGGINGPP

void print_choice()
{
    std::cout << BOLDYELLOW << "Press one of the following keys:" << std::endl;
    std::cout << "\t[h]elp:  print this list" << std::endl;
    std::cout << "\t[s]tart the monitoring" << std::endl;
    std::cout << "\t[e]nd the monitoring" << std::endl;
    std::cout << "\t[p]aus the monitoring" << std::endl;
    std::cout << "\t[r]resume the monitoring" << std::endl;
    std::cout << "\t[q]uit the server" << std::endl;
    std::cout << "\t[a]bort the client" << std::endl;
    std::cout << "\t[t]oggle \"on/off\" to switch the Outputs on/off" << std::endl;
    std::cout << "\t[i]nitialize \"filename\" to (re-)load the config file" << std::endl;
    std::cout << "\t[c]onfigure to apply the configuration from the current file" << std::endl;
    std::cout << "\t[o]utput \"filename\" to change the name of the logfile - only valid after the monitoring has stopped and started again" << std::endl;
    //std::cout << "\t[r]eset to reset the instrument to default state"  << std::endl;
    std::cout << "\t[m]measure to force a read of the latest values and display them"  << std::endl;
    std::cout << "\t[g]et values to print the last measured values to std::cout"  << RESET << std::endl;
}

//pass a std::ofstream object to this and use it to create the response string
bool command_processor (HMP4040Client* pClient, std::vector<std::string>& pInput, std::ostream& os )
{
    bool cQuit = false;

    //all options without arguments
    if (pInput.size() == 1)
    {
        //print help dialog
        if (pInput.at (0) == "h")
            print_choice();

        if (pInput.at (0) == "a")
            cQuit = true;
        // start monitoring workloop
        else if (pInput.at (0) == "s")
            pClient->StartMonitoring ();

        //end monitoring
        else if (pInput.at (0) == "e")
            pClient->StopMonitoring();
        //pause the monitoring
        else if (pInput.at (0) == "p")
            pClient->PauseMonitoring();
        //hard-reset the instrument
        else if (pInput.at (0) == "r")
            pClient->ResumeMonitoring();

        //hard-reset the instrument
        //else if (pInput.at (0) == "r")
        //pClient->Reset();

        //re configure from config file in memory
        else if (pInput.at (0) == "c")
            pClient->Configure();

        //quit application
        else if (pInput.at (0) == "q")
        {
            cQuit = true;
            pClient->Quit();
        }

        // get the latest measured values
        else if (pInput.at (0) == "g")
        {
            std::cout << "Requested latest values from Server ... " << std::endl;
            pClient->GetLatestReadValues();
            MeasurementValues cValues = pClient->fValues;

            // put the Values in os so I can send it via zmq if necessary, otherwise it will just pollute std::cout
            os << "Values: " << cValues.fTimestamp << "\t" << cValues.fVoltages.at (0) << "\t" << cValues.fCurrents.at (0) << "\t" << cValues.fVoltages.at (1) << "\t" << cValues.fCurrents.at (1) << "\t" << cValues.fVoltages.at (2) << "\t" << cValues.fCurrents.at (2) << "\t" << cValues.fVoltages.at (3) << "\t" << cValues.fCurrents.at (3);

        }

        //force a measurement and get the values
        else if (pInput.at (0) == "m")
        {
            std::cout << "Requested reading of values from instrument ... " << std::endl;

            if (pClient->MeasureValues() )
            {
                MeasurementValues cValues = pClient->fValues;

                // put the Values in os so I can send it via zmq if necessary, otherwise it will just pollute std::cout
                os << "Values: " << cValues.fTimestamp << "\t" << cValues.fVoltages.at (0) << "\t" << cValues.fCurrents.at (0) << "\t" << cValues.fVoltages.at (1) << "\t" << cValues.fCurrents.at (1) << "\t" << cValues.fVoltages.at (2) << "\t" << cValues.fCurrents.at (2) << "\t" << cValues.fVoltages.at (3) << "\t" << cValues.fCurrents.at (3);
            }
            else os << "Did not receive a reply!" << std::endl;
        }

        else
            os << "Unknown command" << std::endl;
    }
    else if (pInput.size() == 2)
    {
        //toggle output on or off
        if (pInput.at (0) == "t")
        {
            if (pInput.at (1) == "on" || pInput.at (1) == "ON")
                pClient->ToggleOutput (true);
            else if (pInput.at (1) == "off" || pInput.at (1) == "OFF")
                pClient->ToggleOutput (false);
            else os << "client " << "Error, unknown output state!" << std::endl;
        }

        //init new config file
        else if (pInput.at (0) == "i")
            pClient->SetConfigFileName (pInput.at (1) );

        //new logfile name
        else if (pInput.at (0) == "o")
            pClient->SetLogFileName (pInput.at (1) );
        else
            os << "Unknown command" << std::endl;
    }
    else
        os << "Too many arguments!" << std::endl;

    return cQuit;
}

std::vector<std::string> tokenize_input ( std::string& cInput )
{
    //add some code that get's cin from shell and returns a vector of strings
    std::vector<std::string> cOutput;

    boost::char_separator<char> sep (" ");
    boost::tokenizer<boost::char_separator<char>> tokens (cInput, sep);

    for (const auto& t : tokens)
        cOutput.push_back (t);

    //std::cout << std::endl << "\r";
    return cOutput;
}

void workloop_local (HMP4040Client* pClient)
{
    bool cQuit = false;
    print_choice();

    while (!cQuit)
    {
        std::cout << ">";
        std::string cInput = "";
        std::getline (std::cin, cInput);
        std::vector<std::string> cOutput = tokenize_input (cInput);
        std::cout << std::endl << "\r";

        std::stringstream ss;
        cQuit = command_processor (pClient, cOutput, ss);
        LOG (INFO) << ss.str();

    }
}



int main (int argc, char** argv)
{
    //configure the logger
    el::Configurations conf ("settings/logger.conf");
    el::Loggers::reconfigureAllLoggers (conf);

    //CMD line parsing goes here!
    ArgvParser cmd;

    // init
    cmd.setIntroductoryDescription ( "CMS Ph2_USBInstument Driver and Monitoring library") ;
    // error codes
    cmd.addErrorCode ( 0, "Success" );
    cmd.addErrorCode ( 1, "Error" );
    // options
    cmd.setHelpOption ( "h", "help", "Print this help page" );


    cmd.defineOption ( "file", "Hw Description File . Default value: settings/HMP4040.xml", ArgvParser::OptionRequiresValue /*| ArgvParser::OptionRequired*/ );
    cmd.defineOptionAlternative ( "file", "f" );

    cmd.defineOption ( "save", "Save the data to a text file. Default value: LV_log.txt", ArgvParser::OptionRequiresValue );
    cmd.defineOptionAlternative ( "save", "s" );

    cmd.defineOption ( "hostname", "Hostname of the server - default \"localhost\"", ArgvParser::OptionRequiresValue );
    cmd.defineOptionAlternative ( "hostname", "H" );

    cmd.defineOption ( "port", "port of the server - default value: 8081", ArgvParser::OptionRequiresValue );
    cmd.defineOptionAlternative ( "port", "p" );

    int result = cmd.parse ( argc, argv );

    if ( result != ArgvParser::NoParserError )
    {
        std::cout << cmd.parseErrorDescription ( result );
        exit ( 1 );
    }

    // now query the parsing results
    std::string cHWFile = ( cmd.foundOption ( "file" ) ) ? cmd.optionValue ( "file" ) : "settings/HMP4040.xml";
    std::string cOutputFile = ( cmd.foundOption ( "save" ) ) ? cmd.optionValue ( "save" ) : "LV_log.txt";
    std::string cHostname = (cmd.foundOption ("hostname") ) ? cmd.optionValue ("hostname") : "localhost";
    int cPort = (cmd.foundOption ("port") ) ? atoi (cmd.optionValue ("port").c_str() ) : 8081;

    LOG (INFO) << BOLDYELLOW << "Saving LV data to:   " << BLUE << cOutputFile << RESET;

    //here eventually configure THttpServer for GUI
    char hostname[HOST_NAME_MAX];
    gethostname (hostname, HOST_NAME_MAX);

    LOG (INFO) << BOLDBLUE << "Running HMP404Client on " << BOLDGREEN <<  hostname << RESET;
    HMP4040Client* cClient = new HMP4040Client (cHostname, cPort);
    workloop_local (cClient);

    if (cClient) delete cClient;

    return 0;
}
